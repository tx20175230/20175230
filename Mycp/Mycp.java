import java.io.*;
public class Mycp {
    public static void main(String[] args) throws Exception{
        String x, y, a, result = "", num;

        a = args[0];
        x = args[1];
        y = args[2];

        try {
            FileInputStream fis = new FileInputStream(x);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader in = new BufferedReader(isr);
            PrintStream ps = new PrintStream(y);
            num = in.readLine();

            if (a.equals("-xt")) {
                result = Integer.valueOf(num, 2).toString();
            } else if (a.equals("-tx")) {
                int n, temp = Integer.parseInt(num);
                for (int i = temp; i > 0; i = i / 2) {
                    if (i % 2 == 0) {
                        n = 0;
                    } else {
                        n = 1;
                    }
                    result = n + result;
                }
            } else {
                System.out.println("Error!");
            }
            ps.append(result);
            ps.flush();
            ps.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
