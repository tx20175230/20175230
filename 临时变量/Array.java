import java.util.Arrays;
public class Array {
    public static void main(String[] args) {
         //定义一个数组
        int arr[] = {1,2,3,4,5,6,7,8};

       //打印原始数组的值
        for(int i:arr){
            System.out.print(i + " ");
        }
        System.out.println();

        // 删除上面数组中的5
        for(int i:arr){
            if(arr[i-1]==5)
                while (i < arr.length) {
                    arr[i - 1] = arr[i];
                    i++;
                }
                // for (int j = i ; i < arr.length-1; i++) {
                //    arr[i - 1] = arr[j];
                // }
        }
        arr[arr.length - 1]=0;
        //数组缩容
        // arr = Arrays.copyOf(arr, arr.length-1);

        //打印出 1 2 3 4 6 7 8 0
        for(int i:arr){
            System.out.print(i + " ");
        }
        System.out.println();

        // 添加代码再在4后面5
        for(int i:arr) {
            if (arr[i - 1] == 4) {
                int j = arr.length - 1;
                while (j > i ) {
                    arr[j] = arr[j - 1];
                    j--;
                }
                arr[i] = 5;
            }
        }


        //打印出 1 2 3 4 5 6 7 8
        for(int i:arr){
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
