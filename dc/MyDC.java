import java.util.Stack;
import java.util.StringTokenizer;
import java.util.*;
public class MyDC
{
    /** constant for addition symbol */
    private final char ADD = '+';
    /** constant for subtraction symbol */
    private final char SUBTRACT = '-';
    /** constant for multiplication symbol */
    private final char MULTIPLY = '*';
    /** constant for division symbol */
    private final char DIVIDE = '/';
    /** the stack */
    private Stack<Integer> stack;
   // MyDC my= new MyDC();
    public MyDC() {
        stack = new Stack<Integer>();
    }

    public int evaluate (String expr)
    {
        int op1, op2, result = 0;
        String token;
        StringTokenizer tokenizer = new StringTokenizer (expr);
        while (tokenizer.hasMoreTokens())
            {
            token = tokenizer.nextToken();
                //System.out.println(token);
            //如果是运算符，调用isOperator
            if (isOperator(token))
            {
                char []c=token.toCharArray();
                op1=stack.pop();
                //从栈中弹出操作数2
                op2=stack.pop();
                //从栈中弹出操作数1
                result=evalSingleOp(c[0],op1,op2);
                //根据运算符和两个操作数调用evalSingleOp计算result;
                stack.push(result);
                //计算result入栈;
            }
            else{
                //如果是操作数
                if(token.matches("[0-9]+")) {
                    int number=Integer.parseInt(token);
                    stack.push(number);
                }
            }
        }
        return result;
    }

    private boolean isOperator (String token)
    {
        return ( token.equals("+") || token.equals("-") ||
                token.equals("*") || token.equals("/") );
    }

    private int evalSingleOp (char operation, int op1, int op2)
    {
        int result = 0;

        switch (operation)
        {
            case ADD:
                result = op1 + op2;
                break;
            case SUBTRACT:
                result = op1 - op2;
                break;
            case MULTIPLY:
                result = op1 * op2;
                break;
            case DIVIDE:
                result = op1 / op2;
        }

        return result;
    }
}
